//! PETSc viewers export information and data from PETSc objects.
//!
//! PETSc C API docs: <https://petsc.org/release/docs/manualpages/Viewer/index.html>

use crate::prelude::*;

/// Way a [`Viewer`] presents the object.
pub use petsc_sys::PetscViewerFormat;

/// [`Viewer`] Type
pub use crate::petsc_raw::ViewerTypeEnum as ViewerType;

/// Abstract collection of PetscViewers. It is just an expandable array of viewers.
// TODO: right now this is a very basic wrapper of the view functionality, I feel like
// we could do this more rusty, but i don't really know how.
// It might make sense to make our own viewer code. That we can do everything on the rust side
// This would work more nicely with things like display and debug, also the file system.
// for now we will just use the C api.
pub struct Viewer<'a> {
    pub(crate) ptr: *mut petsc_raw::_p_PetscViewer,
    _lifetime: PhantomData<&'a ()>,
}

impl<'a> Viewer<'a> {
    pub(crate) fn new(ptr: *mut petsc_raw::_p_PetscViewer) -> Self {
        Self {
            ptr,
            _lifetime: PhantomData,
        }
    }

    /// Creates a ASCII PetscViewer shared by all processors in a communicator.
    pub fn create_ascii_stdout(comm: impl Into<CommOpt>) -> crate::Result<Self> {
        // Note, `PetscViewerASCIIGetStdout` calls `PetscObjectRegisterDestroy` which will cause
        // the object to be destroyed when `PetscFinalize()` is called. That is why we increase the
        // reference count.
        let comm = comm.into().as_raw();
        let mut ptr = MaybeUninit::uninit();
        let ierr = unsafe { petsc_sys::PetscViewerASCIIGetStdout(comm, ptr.as_mut_ptr()) };
        crate::Petsc::check_error(comm, ierr)?;

        let mut viewer = Viewer::new(unsafe { ptr.assume_init() });
        unsafe { viewer.reference()? };

        Ok(viewer)
    }

    /// Determines whether a PETSc [`Viewer`] is of a particular type.
    pub fn type_compare(&self, type_kind: ViewerType) -> crate::Result<bool> {
        self.type_compare_str(&type_kind.to_string())
    }

    /// Views a [viewable PetscObject](PetscViewable).
    ///
    /// Same as [`obj.view_with(&self)`](PetscViewable::view_with())
    pub fn view<T: PetscViewable>(&self, obj: &T) -> crate::Result<()> {
        obj.view_with(self)
    }

    wrap_simple_petsc_member_funcs! {
        PetscViewerPushFormat, pub push_format, input PetscViewerFormat, format, takes mut, #[doc = "Sets the format for file PetscViewers."];
    }
}

/// A PETSc object that can be viewed with a [`Viewer`]
pub trait PetscViewable {
    /// Views the object with a [`Viewer`]
    fn view_with<'vl, 'val: 'vl>(
        &self,
        viewer: impl Into<Option<&'vl Viewer<'val>>>,
    ) -> crate::Result<()>;
}

impl_petsc_object_traits! { Viewer, ptr, petsc_raw::_p_PetscViewer, PetscViewerView, PetscViewerDestroy; }

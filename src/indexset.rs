//! Index set (IS) objects are used to index into vectors and matrices and to setup vector scatters.
//!
//! PETSc C API docs: <https://petsc.org/release/docs/manualpages/IS/index.html>

use crate::prelude::*;

/// [`IS`] Type
pub use crate::petsc_raw::ISTypeEnum as ISType;

/// Abstract PETSc object that allows indexing.
pub struct IS<'a> {
    pub(crate) ptr: *mut petsc_raw::_p_IS,
    _lifetime: PhantomData<&'a ()>,
}

/// A immutable view of the indices with Deref to slice.
pub struct ISView<'a, 'b> {
    is: &'b IS<'a>,
    array: *const Int,
    // Or should this just be an ndarray
    // pub(crate) ndarray: ArrayView<'b, Int, ndarray::Ix1>,
    slice: &'b [Int],
}

impl Drop for ISView<'_, '_> {
    fn drop(&mut self) {
        let ierr = unsafe { petsc_raw::ISRestoreIndices(self.is.ptr, &mut self.array as *mut _) };
        self.is
            .check_error(ierr)
            .expect("failed to restore IS indices");
    }
}

impl<'a> IS<'a> {
    pub(crate) fn new(ptr: *mut petsc_raw::_p_IS) -> Self {
        Self {
            ptr,
            _lifetime: PhantomData,
        }
    }

    /// Creates an index set object.
    pub fn create(comm: impl Into<CommOpt>) -> crate::Result<Self> {
        let comm = comm.into().as_raw();
        let mut ptr = MaybeUninit::uninit();
        let ierr = unsafe { petsc_raw::ISCreate(comm, ptr.as_mut_ptr()) };
        crate::Petsc::check_error(comm, ierr)?;
        Ok(IS::new(unsafe { ptr.assume_init() }))
    }

    /// Returns [`ISView`] that derefs into a slice of the indices.
    pub fn indices(&self) -> crate::Result<ISView<'a, '_>> {
        ISView::create(self)
    }

    /// Determines whether a PETSc [`IS`] is of a particular type.
    pub fn type_compare(&self, type_kind: ISType) -> crate::Result<bool> {
        self.type_compare_str(&type_kind.to_string())
    }
}

impl<'a, 'b> ISView<'a, 'b> {
    /// Constructs a ISView from a IS reference
    fn create(is: &'b IS<'a>) -> crate::Result<Self> {
        let mut array = MaybeUninit::<*const Int>::uninit();
        let ierr = unsafe { petsc_raw::ISGetIndices(is.ptr, array.as_mut_ptr()) };
        is.check_error(ierr)?;

        // let ndarray = unsafe {
        //     ArrayView::from_shape_ptr(ndarray::Ix1(is.local_size()? as usize), array.assume_init()) };
        let slice =
            unsafe { std::slice::from_raw_parts(array.assume_init(), is.local_size()? as usize) };

        // Ok(Self { is, array: unsafe { array.assume_init() }, ndarray })
        Ok(Self {
            is,
            array: unsafe { array.assume_init() },
            slice,
        })
    }
}

impl<'b> Deref for ISView<'_, 'b> {
    // type Target = ArrayView<'b, Int, ndarray::Ix1>;
    // fn deref(&self) -> &ArrayView<'b, Int, ndarray::Ix1> {
    //     &self.ndarray
    // }
    type Target = [Int];
    fn deref(&self) -> &'b [Int] {
        &self.slice
    }
}

impl std::fmt::Debug for ISView<'_, '_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.slice.fmt(f)
    }
}

// macro impls
impl IS<'_> {
    wrap_simple_petsc_member_funcs! {
        ISGetLocalSize, pub local_size, output usize, size, #[doc = "Returns the local (processor) length of an index set. "];
    }
}

impl_petsc_object_traits! { IS, ptr, petsc_raw::_p_IS, ISView, ISDestroy; }

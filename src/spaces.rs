//! PETSc [`Space`] struct is used to encapsulate a function space.
//!
//! PETSc C API docs: <https://petsc.org/release/docs/manualpages/SPACE/index.html>

use crate::prelude::*;

/// [`DualSpace`] type
pub use petsc_raw::PetscDualSpaceTypeEnum as DualSpaceType;
/// [`Space`] type
pub use petsc_raw::PetscSpaceTypeEnum as SpaceType;

/// Abstract PETSc object that encapsulates a function space.
pub struct Space<'a> {
    pub(crate) ptr: *mut petsc_raw::_p_PetscSpace,
    _lifetime: PhantomData<&'a ()>,
}

/// PETSc object that manages the dual space to a linear space, e.g. the space of evaluation functionals at the vertices of a triangle
pub struct DualSpace<'a, 'tl> {
    pub(crate) ptr: *mut petsc_raw::_p_PetscDualSpace,
    _lifetime: PhantomData<&'a ()>,

    dm: Option<DM<'a, 'tl>>,
}

impl<'a> Space<'a> {
    pub(crate) fn new(ptr: *mut petsc_raw::_p_PetscSpace) -> Self {
        Self {
            ptr,
            _lifetime: PhantomData,
        }
    }

    /// Creates an empty [`Space`] object.
    ///
    /// The type can then be set with [`Space::set_type()`].
    pub fn create(comm: impl Into<CommOpt>) -> crate::Result<Self> {
        let comm = comm.into().as_raw();
        let mut ptr = MaybeUninit::uninit();
        let ierr = unsafe { petsc_raw::PetscSpaceCreate(comm, ptr.as_mut_ptr()) };
        crate::Petsc::check_error(comm, ierr)?;
        Ok(Space::new(unsafe { ptr.assume_init() }))
    }

    /// Builds a particular [`Space`] (given as `&str`).
    pub fn set_type_str(&mut self, space_type: &str) -> crate::Result<()> {
        let cstring = CString::new(space_type).expect("`CString::new` failed");
        let ierr = unsafe { petsc_raw::PetscSpaceSetType(self.ptr, cstring.as_ptr()) };
        self.check_error(ierr)
    }

    /// Builds a particular [`Space`].
    pub fn set_type(&mut self, space_type: SpaceType) -> crate::Result<()> {
        // This could be use the macro probably
        let option_str = petsc_raw::PETSCSPACETYPE_TABLE[space_type as usize];
        let ierr =
            unsafe { petsc_raw::PetscSpaceSetType(self.ptr, option_str.as_ptr() as *const _) };
        self.check_error(ierr)
    }

    /// Determines whether a PETSc [`Space`] is of a particular type.
    pub fn type_compare(&self, type_kind: SpaceType) -> crate::Result<bool> {
        self.type_compare_str(&type_kind.to_string())
    }

    /// Set the degree of approximation for this space.
    ///
    /// # Parameters
    ///
    /// * `degree` - The degree of the largest polynomial space contained in the space.
    /// * `max_degree` - The degree of the largest polynomial space containing the space.
    ///
    /// One of `degree` or `max_degree` can be `None` to have PETSc determine what it should be.
    pub fn set_degree(
        &mut self,
        degree: impl Into<Option<Int>>,
        max_degree: impl Into<Option<Int>>,
    ) -> crate::Result<()> {
        let ierr = unsafe {
            petsc_raw::PetscSpaceSetDegree(
                self.as_raw(),
                degree.into().unwrap_or(petsc_raw::PETSC_DETERMINE),
                max_degree.into().unwrap_or(petsc_raw::PETSC_DETERMINE),
            )
        };
        self.check_error(ierr)
    }
}

impl<'a, 'tl> DualSpace<'a, 'tl> {
    pub(crate) fn new(ptr: *mut petsc_raw::_p_PetscDualSpace) -> Self {
        Self {
            ptr,
            _lifetime: PhantomData,
            dm: None,
        }
    }

    /// Creates an empty [`DualSpace`] object.
    ///
    /// The type can then be set with [`DualSpace::set_type()`].
    pub fn create(comm: impl Into<CommOpt>) -> crate::Result<Self> {
        let comm = comm.into().as_raw();
        let mut ptr = MaybeUninit::uninit();
        let ierr = unsafe { petsc_raw::PetscDualSpaceCreate(comm, ptr.as_mut_ptr()) };
        crate::Petsc::check_error(comm, ierr)?;
        Ok(DualSpace::new(unsafe { ptr.assume_init() }))
    }

    /// Builds a particular [`DualSpace`] (given as `&str`).
    pub fn set_type_str(&mut self, ds_type: &str) -> crate::Result<()> {
        let cstring = CString::new(ds_type).expect("`CString::new` failed");
        let ierr = unsafe { petsc_raw::PetscDualSpaceSetType(self.ptr, cstring.as_ptr()) };
        self.check_error(ierr)
    }

    /// Builds a particular [`DualSpace`].
    pub fn set_type(&mut self, ds_type: DualSpaceType) -> crate::Result<()> {
        // This could be use the macro probably
        let option_str = petsc_raw::PETSCDUALSPACETYPE_TABLE[ds_type as usize];
        let ierr =
            unsafe { petsc_raw::PetscDualSpaceSetType(self.ptr, option_str.as_ptr() as *const _) };
        self.check_error(ierr)
    }

    /// Determines whether a PETSc [`DualSpace`] is of a particular type.
    pub fn type_compare(&self, type_kind: DualSpaceType) -> crate::Result<bool> {
        self.type_compare_str(&type_kind.to_string())
    }
}

// macro impls
impl<'a> Space<'a> {
    wrap_simple_petsc_member_funcs! {
        PetscSpaceSetFromOptions, pub set_from_options, takes mut, #[doc = "Sets parameters from the options database"];
        PetscSpaceSetUp, pub set_up, takes mut, #[doc = "Construct data structures"];
        PetscSpacePolynomialSetTensor, pub polynomial_set_tensor, input bool, tensor, takes mut, #[doc = "Set whether a function space is a space of tensor polynomials, as opposed to polynomials.\n\n\
            For tensor polynomials the space is spanned by polynomials whose degree in each variable is bounded by the given order. For polynomials the space is spanned by polynomials whose total degree---summing over all variables---is bounded by the given order."];
        PetscSpacePolynomialGetTensor, pub iptrolynomial_tensor, output bool, tensor, #[doc = "Gets whether a function space is a space of tensor polynomials, as opposed to polynomials."];
        PetscSpaceSetNumComponents, pub set_num_components, input Int, nc, takes mut, #[doc = "Set the number of components for this space"];
        PetscSpaceSetNumVariables, pub set_num_variables, input Int, nv, takes mut, #[doc = "Set the number of variables for this space"];
        PetscSpaceGetNumComponents, pub num_components, output usize, nc, #[doc = "Get the number of components for this space"];
        PetscSpaceGetNumVariables, pub num_variables, output usize, nv, #[doc = "Get the number of variables for this space"];
        PetscSpaceGetDegree, pub degree, output usize, degree, output usize, max_degree, #[doc = "Get the degree of approximation for this space."];
    }
}

impl<'a, 'tl> DualSpace<'a, 'tl> {
    wrap_simple_petsc_member_funcs! {
        PetscDualSpaceSetFromOptions, pub set_from_options, takes mut, #[doc = "Sets parameters from the options database"];
        PetscDualSpaceSetUp, pub set_up, takes mut, #[doc = "Construct data structures"];
        PetscDualSpaceLagrangeSetTensor, pub lagrange_set_tensor, input bool, tensor, takes mut, #[doc = "Set the tensor nature of the dual space.\n\n\
            Whether the dual space has tensor layout (vs. simplicial)"];
        PetscDualSpaceLagrangeGetTensor, pub is_lagrange_tensor, output bool, tensor, #[doc = "Gets whether the dual space has tensor layout (vs. simplicial)"];
        PetscDualSpaceSetNumComponents, pub set_num_components, input Int, nc, takes mut, #[doc = "Set the number of components for this space"];
        PetscDualSpaceSetOrder, pub set_order, input Int, degree, takes mut, #[doc = "Set the order of the dual space."];
        PetscDualSpaceGetNumComponents, pub num_components, output usize, nc, #[doc = "Get the number of components for this space"];
        PetscDualSpaceGetOrder, pub order, output usize, degree, #[doc = "Get the order of the dual space."];
        PetscDualSpaceSetDM, pub set_dm, input DM<'a, 'tl>, dm .as_raw consume .dm, takes mut, #[doc = "Set the DM representing the reference cell "];
    }
}

impl_petsc_object_traits! {
    Space, ptr, petsc_raw::_p_PetscSpace, PetscSpaceView, PetscSpaceDestroy;
    DualSpace, ptr, petsc_raw::_p_PetscDualSpace, PetscDualSpaceView, PetscDualSpaceDestroy, '_;
}

//! This file will show how to do the kps ex1 example in rust using the petsc-rs bindings.
//!
//! Concepts: KSP^solving a system of linear equations
//! Processors: 1
//!
//! Use "petsc::prelude::*" to get direct access to all important petsc-rs bindings.
//! Use "mpi::traits::*" to get access to all mpi traits which allow you to call things like `world.size()`.
//!
//! To run:
//! ```text
//! $ cargo build --bin ksp-ex1
//! $ target/debug/ksp-ex1
//! Norm of error 2.41202e-15, Iters 5
//! $ mpiexec -n 1 target/debug/ksp-ex1
//! Norm of error 2.41202e-15, Iters 5
//! $ target/debug/ksp-ex1 -n 100
//! Norm of error 1.14852e-2, Iters 318
//! ```
//!
//! To build for complex you can use the flag `--features petsc-use-complex-unsafe`
//!
//! Note:  The corresponding parallel example is ex23.rs

static HELP_MSG: &str = "Solves a tridiagonal linear system with KSP.\n\n";

use mpi::traits::*;
use petsc::prelude::*;

struct Opt {
    n: usize,
    show_solution: bool,
    verbose: bool,
}

impl petsc::Opt for Opt {
    fn from_opt_builder(pob: &mut petsc::OptBuilder) -> petsc::Result<Self> {
        let n = pob.options_usize("-n", "", "ksp-ex1", 10)?;
        let show_solution = pob.options_bool("-show_solution", "", "ksp-ex1", false)?;
        let verbose = pob.options_bool("-verbose", "", "ksp-ex1", true)?;
        Ok(Opt {
            n,
            show_solution,
            verbose,
        })
    }
}

fn main() -> petsc::Result<()> {
    let petsc = Petsc::builder()
        .args(std::env::args())
        .help_msg(HELP_MSG)
        .init()?;

    let opt = petsc.options()?;

    example_01(&petsc, opt)
}

fn example_01(petsc: &Petsc, opt: Opt) -> petsc::Result<()> {
    let Opt {
        n,
        show_solution,
        verbose,
    } = opt;
    if petsc.world().size() != 1 {
        Petsc::set_error(
            petsc.world(),
            ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
            "This is a uniprocessor example only!",
        )?;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //   Compute the matrix and right-hand-side vector that define
    //   the linear system, Ax = b.
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // Create vectors.  Note that we form 1 vector from scratch and
    // then duplicate as needed.
    let mut x = petsc.vec_create()?;
    x.set_name("Solution")?;
    x.set_sizes(None, n)?;
    x.set_from_options()?;
    let mut b = x.clone();
    let mut u = x.clone();

    #[allow(non_snake_case)]
    let mut A = petsc.mat_create()?;
    A.set_sizes(None, None, n, n)?;
    A.set_from_options()?;
    A.set_up()?;

    // Assemble matrix:
    A.assemble_with(
        (0..n as Int)
            .map(|i| (-1..=1).map(move |j| (i, i + j)))
            .flatten()
            // we could also filter out negatives, but `assemble_with` does that for us
            .filter(|&(i, j)| i < n as Int && j < n as Int)
            .map(|(i, j)| {
                if i == j {
                    (i, j, Scalar::from(2.0))
                } else {
                    (i, j, Scalar::from(-1.0))
                }
            }),
        InsertMode::INSERT_VALUES,
        MatAssemblyType::MAT_FINAL_ASSEMBLY,
    )?;

    // Set exact solution; then compute right-hand-side vector.
    u.set_all(Scalar::from(1.0))?;
    Mat::mult(&A, &u, &mut b)?;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //          Create the linear solver and set various options
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    let mut ksp = petsc.ksp_create()?;

    // Set operators. Here the matrix that defines the linear system
    // also serves as the matrix that defines the preconditioner.
    ksp.set_operators(&A, &A)?;

    // Set linear solver defaults for this problem (optional).
    // - By extracting the KSP and PC contexts from the KSP context,
    //     we can then directly call any KSP and PC routines to set
    //     various options.
    // - The following statements are optional; all of these
    //     parameters could alternatively be specified at runtime via
    //     `KSP::set_from_options()`.
    let pc = ksp.pc_or_create()?;
    pc.set_type(PCType::PCJACOBI)?;
    ksp.set_tolerances(1.0e-5, None, None, None)?;

    // Set runtime options, e.g.,
    //     `-ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>`
    // These options will override those specified above as long as
    // `KSP::set_from_options()` is called _after_ any other customization
    // routines.
    ksp.set_from_options()?;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //                  Solve the linear system
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ksp.solve(&b, &mut x)?;

    // View solver info; we could instead use the option -ksp_view to
    // print this info to the screen at the conclusion of `KSP::solve()`.
    let viewer = Viewer::create_ascii_stdout(petsc.world())?;
    if verbose {
        viewer.view(&ksp)?;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //                Check the solution and clean up
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if show_solution {
        viewer.view(&x)?;
        // Or we can do the following. Note, in a multi-process comm
        // world we should instead use `petsc_println_sync!`.
        println!("{}: {:.2}", x.name()?, *x.view()?);
    }
    x.axpy(Scalar::from(-1.0), &u)?;
    let x_norm = x.norm(NormType::NORM_2)?;
    let iters = ksp.iteration_number()?;
    if show_solution {
        petsc_println!(
            petsc.world(),
            "Norm of error {:.5e}, Iters {}",
            x_norm,
            iters
        )?;
    }
    if x_norm > 1E-13 {
        Petsc::set_error(
            petsc.world(),
            ErrorKind::PETSC_ERR_NOT_CONVERGED,
            "Large error in computed solution",
        )?;
    }

    // All PETSc objects are automatically destroyed when they are no longer needed.
    // PetscFinalize() is also automatically called.

    // return
    Ok(())
}

// ----------------------------------------------------------------------------
// Tests
// ----------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ksp_example_01() {
        let petsc = Petsc::init_no_args().expect("failed to initalize PETSc");

        let opt = Opt {
            n: 10,
            show_solution: false,
            verbose: false,
        };
        assert!(example_01(&petsc, opt).is_ok());
    }
}

//! Concepts: SNES^basic uniprocessor example
//! Concepts: SNES^setting a user-defined monitoring routine
//! Processors: 1
//!
//! To run:
//! ```text
//! $ cargo build --bin snes-ex2
//! $ mpiexec -n 1 target/debug/snes-ex2
//! Norm of error 1.49751e-10, Iters 3
//! ```

static HELP_MSG: &str = "Newton method to solve u'' + u^{2} = f, sequentially.\n\
This example employs a user-defined monitoring routine.\n\n";

use mpi::traits::*;
use petsc::prelude::*;

struct Opt {
    n: usize,
    verbose: bool,
}

impl petsc::Opt for Opt {
    fn from_opt_builder(pob: &mut petsc::OptBuilder) -> petsc::Result<Self> {
        let n = pob.options_usize("-n", "", "snes-ex2", 5)?;
        let verbose = pob.options_bool("-verbose", "", "snes-ex2", true)?;
        Ok(Opt { n, verbose })
    }
}

fn main() -> petsc::Result<()> {
    // optionally initialize mpi
    // let _univ = mpi::initialize().unwrap();
    // init with no options
    let petsc = Petsc::builder()
        .args(std::env::args())
        .help_msg(HELP_MSG)
        .init()?;

    // or init with no options
    // let petsc = Petsc::init_no_args()?;

    let opt = petsc.options()?;

    example_02(&petsc, opt)
}

fn example_02(petsc: &Petsc, opt: Opt) -> petsc::Result<()> {
    let Opt { n, verbose } = opt;

    if petsc.world().size() != 1 {
        Petsc::set_error(
            petsc.world(),
            ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
            "This is a uniprocessor example only!",
        )?;
    }

    let h = 1.0 / (Scalar::from(n as Real) - 1.0);

    /*
        Note that we form 1 vector from scratch and then duplicate as needed.
        Set names for some vectors to facilitate monitoring (optional)
    */
    let mut x = petsc.vec_create()?;
    x.set_sizes(None, n)?;
    x.set_from_options()?;
    let mut g = x.duplicate()?;
    let mut u = x.duplicate()?;
    let mut r = x.duplicate()?;
    x.set_name("Approximate Solution")?;
    u.set_name("Exact Solution")?;

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Initialize application:
        Store right-hand-side of PDE and exact solution
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    u.assemble_with(
        (0..n as Int).map(|i| {
            let xp = Scalar::from(i as Real) * h;
            (i, xp.powi(3))
        }),
        InsertMode::INSERT_VALUES,
    )?;
    g.assemble_with(
        (0..n as Int).map(|i| {
            let xp = Scalar::from(i as Real) * h;
            (i, 6.0 * xp + (xp + 1.0e-12).powi(6)) // +1.e-12 is to prevent 0^6
        }),
        InsertMode::INSERT_VALUES,
    )?;

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Set initial guess
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
        Note: The user should initialize the vector, x, with the initial guess
        for the nonlinear solver prior to calling SNESSolve().  In particular,
        to employ an initial guess of zero, the user should explicitly set
        this vector to zero by calling VecSet().
    */
    x.set_all(Scalar::from(0.5))?;

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Create matrix data structure; set Jacobian evaluation routine
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    #[allow(non_snake_case)]
    let mut J = petsc.mat_create()?;
    J.set_sizes(None, None, n as usize, n as usize)?;
    J.set_from_options()?;
    J.seq_aij_set_preallocation(3, None)?;

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Create nonlinear solver context
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    let mut snes = petsc.snes_create()?;

    /*
        Set function evaluation routine and vector
    */
    snes.set_function(&mut r, |_snes, x: &Vector, f: &mut Vector| {
        /*
            Evaluates nonlinear function, F(x).

            Input Parameters:
            .  snes - the SNES context
            .  x - input vector

            Output Parameter:
            .  f - function vector
        */

        let x_view = x.view()?;
        let mut f_view = f.view_mut()?;
        let g_view = g.view()?;

        let d = Scalar::from(n as Real - 1.0).powi(2);

        f_view[0] = x_view[0];
        for i in 1..(n as usize - 1) {
            f_view[i] = d * (x_view[i - 1] - 2.0 * x_view[i] + x_view[i + 1])
                + x_view[i] * x_view[i]
                - g_view[i];
        }
        f_view[n as usize - 1] = x_view[n as usize - 1] - 1.0;

        Ok(())
    })?;

    /*
        Set Jacobian matrix data structure and default Jacobian evaluation
        routine. User can override with:
        -snes_fd : default finite differencing approximation of Jacobian
        -snes_mf : matrix-free Newton-Krylov method with no preconditioning
                    (unless user explicitly sets preconditioner)
        -snes_mf_operator : form preconditioning matrix as set by the user,
                            but use matrix-free approx for Jacobian-vector
                            products within Newton-Krylov method
    */
    snes.set_jacobian_single_mat(&mut J, |_snes, x: &Vector, ap_mat: &mut Mat| {
        /*
            Evaluates Jacobian matrix.

            Input Parameters:
            .  snes - the SNES context
            .  x - input vector

            Output Parameters:
            .  ap_mat - Jacobian matrix (also used as preconditioning matrix)
        */

        let x_view = x.view()?;

        let d = (Scalar::from(n as Real) - 1.0).powi(2);

        ap_mat.assemble_with(
            (0..n as Int)
                .map(|i| {
                    if i == 0 || i == n as Int - 1 {
                        vec![(i, i, Scalar::from(1.0))]
                    } else {
                        vec![
                            (i, i - 1, d),
                            (i, i, -2.0 * d + 2.0 * x_view[i as usize]),
                            (i, i + 1, d),
                        ]
                    }
                })
                .flatten(),
            InsertMode::INSERT_VALUES,
            MatAssemblyType::MAT_FINAL_ASSEMBLY,
        )?;

        Ok(())
    })?;

    // Set an optional user-defined monitoring routine
    snes.monitor_set(|snes, its, fnorm| {
        if verbose {
            petsc_println!(
                petsc.world(),
                "iter: {}, SNES function norm: {:.5e}",
                its,
                fnorm
            )?;
            let x = snes.solution()?;
            x.view_with(None)?;
        }
        Ok(())
    })?;

    /*
        Set SNES/KSP/KSP/PC runtime options, e.g.,
            -snes_view -snes_monitor -ksp_type <ksp> -pc_type <pc>
    */
    snes.set_from_options()?;

    /*
        Print parameters used for convergence testing (optional) ... just
        to demonstrate this routine; this information is also printed with
        the option -snes_view
    */
    let tols = snes.tolerances()?;
    if verbose {
        petsc_println!(
            petsc.world(),
            "atol={:.5e}, rtol={:.5e}, stol={:.5e}, maxit={}, maxf={}",
            tols.0,
            tols.1,
            tols.2,
            tols.3,
            tols.4
        )?;
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Solve Nonlinear System
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    snes.solve(None, &mut x)?;

    /*
        Check the error
    */
    x.axpy(Scalar::from(-1.0), &u)?;
    let x_norm = x.norm(NormType::NORM_2)?;
    let iters = snes.iteration_number()?;
    if verbose {
        petsc_println!(
            petsc.world(),
            "Norm of error {:.5e}, Iters {}",
            x_norm,
            iters
        )?;
    }
    if x_norm > 1E-9 {
        Petsc::set_error(
            petsc.world(),
            ErrorKind::PETSC_ERR_NOT_CONVERGED,
            "Large error in computed solution",
        )?;
    }

    // return
    Ok(())
}

// ----------------------------------------------------------------------------
// Tests
// ----------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn snes_example_02() {
        let petsc = Petsc::init_no_args().expect("failed to initalize PETSc");

        let opt = Opt {
            n: 10,
            verbose: false,
        };
        assert!(example_02(&petsc, opt).is_ok());
    }
}

//! Concepts: SNES^Poisson Problem in 2d and 3d
//! Concepts: SNES^Using a parallel unstructured mesh (DMPLEX)
//! Processors: n
//!
//! This example only works using PETSc `v3.15-dev.0`.
//!
//! To run:
//! ```text
//! $ cargo build --bin snes-ex12
//! $ mpiexec -n 1 target/debug/snes-ex12
//! $ mpiexec -n 1 target/debug/snes-ex12 -run_type test -variable_coefficient field -petscspace_degree 1 -show_initial -show_solution -dm_plex_print_fem 1 -show_opts
//! $ mpiexec -n 1 target/debug/snes-ex12 -show_initial -show_solution -show_opts -field_bc -variable_coefficient coeff_checkerboard_0 -rand -k 2
//! ```

static HELP_MSG: &str = "Poisson Problem in 2d and 3d with simplicial finite elements.\n\
    We solve the Poisson problem in a rectangular\n\
    domain, using a parallel unstructured mesh (DMPLEX) to discretize it.\n\
    This example supports discretized auxiliary fields (conductivity) as well as\n\
    multilevel nonlinear solvers.\n\n\n";

use core::slice;
use std::{
    fmt::{self, Display},
    io::Error,
    ops::DerefMut,
    rc::Rc,
    str::FromStr,
};

use mpi::topology::UserCommunicator;
use mpi::traits::*;
use petsc::prelude::*;
use rand::prelude::*;

#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
#[allow(non_camel_case_types)]
enum BCType {
    NEUMANN,
    DIRICHLET,
    NONE,
}
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
#[allow(non_camel_case_types)]
enum RunType {
    RUN_FULL,
    RUN_EXACT,
    RUN_TEST,
    RUN_PERF,
}
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
#[allow(non_camel_case_types)]
enum CoeffType {
    COEFF_NONE,
    COEFF_ANALYTIC,
    COEFF_FIELD,
    COEFF_NONLINEAR,
    COEFF_CIRCLE,
    COEFF_CROSS,
    COEFF_CHECKERBOARD_0,
    COEFF_CHECKERBOARD_1,
}

impl FromStr for BCType {
    type Err = Error;
    fn from_str(input: &str) -> Result<BCType, Error> {
        match input.to_uppercase().as_str() {
            "NEUMANN" => Ok(BCType::NEUMANN),
            "DIRICHLET" => Ok(BCType::DIRICHLET),
            "NONE" => Ok(BCType::NONE),
            _ => Err(Error::new(
                std::io::ErrorKind::InvalidInput,
                format!("{}, is not valid", input),
            )),
        }
    }
}

impl Display for BCType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            BCType::NEUMANN => write!(f, "Neumann"),
            BCType::DIRICHLET => write!(f, "Dirichlet"),
            _ => write!(f, "None"),
        }
    }
}

impl FromStr for RunType {
    type Err = Error;
    fn from_str(input: &str) -> Result<RunType, Error> {
        match input.to_uppercase().as_str() {
            "FULL" => Ok(RunType::RUN_FULL),
            "EXACT" => Ok(RunType::RUN_EXACT),
            "TEST" => Ok(RunType::RUN_TEST),
            "PERF" => Ok(RunType::RUN_PERF),
            _ => Err(Error::new(
                std::io::ErrorKind::InvalidInput,
                format!("{}, is not valid", input),
            )),
        }
    }
}

impl Display for RunType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            RunType::RUN_FULL => write!(f, "full"),
            RunType::RUN_EXACT => write!(f, "exact"),
            RunType::RUN_TEST => write!(f, "test"),
            _ => write!(f, "perf"),
        }
    }
}

impl FromStr for CoeffType {
    type Err = Error;
    fn from_str(input: &str) -> Result<CoeffType, Error> {
        match input.to_uppercase().as_str() {
            "NONE" => Ok(CoeffType::COEFF_NONE),
            "ANALYTIC" => Ok(CoeffType::COEFF_ANALYTIC),
            "FIELD" => Ok(CoeffType::COEFF_FIELD),
            "NONLINEAR" => Ok(CoeffType::COEFF_NONLINEAR),
            "CIRCLE" => Ok(CoeffType::COEFF_CIRCLE),
            "CROSS" => Ok(CoeffType::COEFF_CROSS),
            "CHECKERBOARD_0" => Ok(CoeffType::COEFF_CHECKERBOARD_0),
            "CHECKERBOARD_1" => Ok(CoeffType::COEFF_CHECKERBOARD_1),
            _ => Err(Error::new(
                std::io::ErrorKind::InvalidInput,
                format!("{}, is not valid", input),
            )),
        }
    }
}

impl Display for CoeffType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CoeffType::COEFF_NONE => write!(f, "NONE"),
            CoeffType::COEFF_ANALYTIC => write!(f, "ANALYTIC"),
            CoeffType::COEFF_FIELD => write!(f, "FIELD"),
            CoeffType::COEFF_NONLINEAR => write!(f, "NONLINEAR"),
            CoeffType::COEFF_CIRCLE => write!(f, "CIRCLE"),
            CoeffType::COEFF_CROSS => write!(f, "CROSS"),
            CoeffType::COEFF_CHECKERBOARD_0 => write!(f, "CHECKERBOARD_0"),
            CoeffType::COEFF_CHECKERBOARD_1 => write!(f, "CHECKERBOARD_1"),
        }
    }
}

#[derive(Debug)]
struct Opt {
    run_type: RunType,
    bc_type: BCType,
    variable_coefficient: CoeffType,
    field_bc: bool,
    jacobian_mf: bool,
    show_initial: bool,
    show_solution: bool,
    nonz_init: bool,
    bd_integral: bool,
    check_ksp: bool,
    div: Int,
    k: Int,
    rand: bool,
    dm_view: bool,
    guess_vec_view: bool,
    vec_view: bool,
    coeff_view: bool,
    show_opts: bool,
    verbose: bool,
}

impl petsc::Opt for Opt {
    fn from_opt_builder(pob: &mut petsc::OptBuilder) -> petsc::Result<Self> {
        Ok(Opt {
            run_type: pob.options_from_string(
                "-run_type",
                "The run type",
                "snes-ex12",
                RunType::RUN_FULL,
            )?,
            bc_type: pob.options_from_string(
                "-bc_type",
                "Type of boundary condition",
                "snes-ex12",
                BCType::DIRICHLET,
            )?,
            variable_coefficient: pob.options_from_string(
                "-variable_coefficient",
                "Type of variable coefficent",
                "snes-ex12",
                CoeffType::COEFF_NONE,
            )?,
            field_bc: pob.options_bool(
                "-field_bc",
                "Use a field representation for the BC",
                "snes-ex12",
                false,
            )?,
            jacobian_mf: pob.options_bool(
                "-jacobian_mf",
                "Calculate the action of the Jacobian on the fly",
                "snes-ex12",
                false,
            )?,
            show_initial: pob.options_bool(
                "-show_initial",
                "Output the initial guess for verification",
                "snes-ex12",
                false,
            )?,
            show_solution: pob.options_bool(
                "-show_solution",
                "Output the solution for verification",
                "snes-ex12",
                false,
            )?,
            nonz_init: pob.options_bool(
                "-nonz_init",
                "nonzero initial guess",
                "snes-ex12",
                false,
            )?,
            bd_integral: pob.options_bool(
                "-bd_integral",
                "Compute the integral of the solution on the boundary",
                "snes-ex12",
                false,
            )?,
            check_ksp: pob.options_bool(
                "-check_ksp",
                "Check solution of KSP",
                "snes-ex12",
                false,
            )?,
            div: pob.options_int(
                "-div",
                "The number of division for the checkerboard coefficient",
                "snes-ex12",
                4,
            )?,
            k: pob.options_int(
                "-k",
                "The exponent for the checkerboard coefficient",
                "snes-ex12",
                1,
            )?,
            rand: pob.options_bool(
                "-rand",
                "Assign random k values to checkerboard",
                "snes-ex12",
                false,
            )?,
            dm_view: pob.options_bool("-dm_view", "", "snes-ex12", false)?,
            guess_vec_view: pob.options_bool("-guess_vec_view", "", "snes-ex12", false)?,
            vec_view: pob.options_bool("-vec_view", "", "snes-ex12", false)?,
            coeff_view: pob.options_bool("-coeff_view", "", "snes-ex12", false)?,
            show_opts: pob.options_bool("-show_opts", "", "snes-ex12", false)?,
            verbose: pob.options_bool("-verbose", "", "snes-ex12", true)?,
        })
    }
}

fn create_mesh<'a, 'b>(
    world: &'a UserCommunicator,
    opt: &Opt,
) -> petsc::Result<(DM<'a, 'b>, Option<Vec<Int>>)> {
    let mut dm = DM::plex_create(world)?;
    dm.set_name("Mesh")?;
    dm.set_from_options()?;

    // TODO: if `-dm_plex_convert_type` flag set

    if opt.dm_view {
        dm.view_with(None)?;
    }
    let kgrid = if opt.rand {
        let dim = dm.dimension()?;
        let n = opt.div.pow(dim as u32);
        let mut rng = StdRng::seed_from_u64(1973);
        Some((0..n).map(|_| rng.gen_range(1..=opt.k)).collect())
    } else {
        None
    };

    Ok((dm, kgrid))
}

// For now, this api does not exist in `petsc-rs` so we have to use petsc-sys
fn petsc_ds_set_residual(
    ds: &mut DS,
    f0: unsafe extern "C" fn(
        Int,
        Int,
        Int,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        Real,
        *const Real,
        Int,
        *const Scalar,
        *mut Scalar,
    ),
    f1: unsafe extern "C" fn(
        Int,
        Int,
        Int,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        Real,
        *const Real,
        Int,
        *const Scalar,
        *mut Scalar,
    ),
) -> petsc::Result<()> {
    let ierr = unsafe {
        petsc_sys::PetscDSSetResidual(
            ds.as_raw(),
            0,
            std::mem::transmute(Some(f0)),
            std::mem::transmute(Some(f1)),
        )
    };
    if ierr == 0 {
        Ok(())
    } else {
        Petsc::set_error(
            ds.comm(),
            ErrorKind::PETSC_ERR_ARG_CORRUPT,
            "PetscDSSetResidual failed",
        )
    }
}

// For now, this api does not exist in `petsc-rs` so we have to use petsc-sys
fn petsc_weak_form_set_index_bd_residual(
    wf: &mut WeakForm,
    label: &DMLabel,
    id: Int,
    f0: unsafe extern "C" fn(
        Int,
        Int,
        Int,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        Real,
        *const Real,
        *const Real,
        Int,
        *const Scalar,
        *mut Scalar,
    ),
) -> petsc::Result<()> {
    let ierr = unsafe {
        petsc_sys::PetscWeakFormSetIndexBdResidual(
            wf.as_raw(),
            label.as_raw(),
            id,
            0,
            0,
            0,
            Some(f0),
            0,
            None,
        )
    };
    if ierr == 0 {
        Ok(())
    } else {
        Petsc::set_error(
            wf.comm(),
            ErrorKind::PETSC_ERR_ARG_CORRUPT,
            "PetscWeakFormSetIndexBdResidual failed",
        )
    }
}

// For now, this api does not exist in `petsc-rs` so we have to use petsc-sys
fn petsc_ds_set_jacobian(
    ds: &mut DS,
    g3: unsafe extern "C" fn(
        Int,
        Int,
        Int,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        *const Int,
        *const Int,
        *const Scalar,
        *const Scalar,
        *const Scalar,
        Real,
        Real,
        *const Real,
        Int,
        *const Scalar,
        *mut Scalar,
    ),
) -> petsc::Result<()> {
    let ierr = unsafe {
        petsc_sys::PetscDSSetJacobian(
            ds.as_raw(),
            0,
            0,
            None,
            None,
            None,
            std::mem::transmute(Some(g3)),
        )
    };
    if ierr == 0 {
        Ok(())
    } else {
        Petsc::set_error(
            ds.comm(),
            ErrorKind::PETSC_ERR_ARG_CORRUPT,
            "PetscDSSetJacobian failed",
        )
    }
}

fn setup_problem(
    dm: &mut DM,
    opt: &Opt,
) -> petsc::Result<(
    fn(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>,
    Option<
        unsafe extern "C" fn(
            Int,
            Int,
            Int,
            *const Int,
            *const Int,
            *const Scalar,
            *const Scalar,
            *const Scalar,
            *const Int,
            *const Int,
            *const Scalar,
            *const Scalar,
            *const Scalar,
            Real,
            *const Real,
            Int,
            *const Scalar,
            *mut Scalar,
        ),
    >,
)> {
    let dim = dm.dimension()?;
    let periodicity = {
        if let Some((_, _, l)) = dm.periodicity()? {
            let mut periodicity = vec![0.; dim];
            periodicity.clone_from_slice(l);
            Some(periodicity)
        } else {
            None
        }
    };
    {
        let mut ds = dm.try_ds_mut().unwrap();

        match opt.variable_coefficient {
            CoeffType::COEFF_NONE => match periodicity {
                Some(ref per_l) if per_l[0] > 0. => {
                    if per_l[1] > 0. {
                        petsc_ds_set_residual(&mut ds, f0_xytrig_u, f1_u)?;
                        petsc_ds_set_jacobian(&mut ds, g3_uu)?;
                    } else {
                        petsc_ds_set_residual(&mut ds, f0_xtrig_u, f1_u)?;
                        petsc_ds_set_jacobian(&mut ds, g3_uu)?;
                    }
                }
                _ => {
                    petsc_ds_set_residual(ds, f0_u, f1_u)?;
                    petsc_ds_set_jacobian(ds, g3_uu)?;
                }
            },
            CoeffType::COEFF_ANALYTIC => {
                petsc_ds_set_residual(ds, f0_analytic_u, f1_analytic_u)?;
                petsc_ds_set_jacobian(ds, g3_analytic_uu)?;
            }
            CoeffType::COEFF_FIELD => {
                petsc_ds_set_residual(ds, f0_analytic_u, f1_field_u)?;
                petsc_ds_set_jacobian(ds, g3_field_uu)?;
            }
            CoeffType::COEFF_NONLINEAR => {
                petsc_ds_set_residual(ds, f0_analytic_nonlinear_u, f1_analytic_nonlinear_u)?;
                petsc_ds_set_jacobian(ds, g3_analytic_nonlinear_uu)?;
            }
            CoeffType::COEFF_CIRCLE => {
                petsc_ds_set_residual(ds, f0_circle_u, f1_u)?;
                petsc_ds_set_jacobian(ds, g3_uu)?;
            }
            CoeffType::COEFF_CROSS => {
                petsc_ds_set_residual(ds, f0_cross_u, f1_u)?;
                petsc_ds_set_jacobian(ds, g3_uu)?;
            }
            CoeffType::COEFF_CHECKERBOARD_0 => {
                petsc_ds_set_residual(ds, f0_checkerboard_0_u, f1_field_u)?;
                petsc_ds_set_jacobian(ds, g3_field_uu)?;
            }
            _ => {
                Petsc::set_error(
                    ds.comm(),
                    ErrorKind::PETSC_ERR_ARG_WRONG,
                    format!(
                        "Invalid variable coefficient type {:?}",
                        opt.variable_coefficient
                    ),
                )?;
            }
        }
    } // drop ds

    let exact_func: fn(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>;
    let mut exact_field: Option<
        unsafe extern "C" fn(
            Int,
            Int,
            Int,
            *const Int,
            *const Int,
            *const Scalar,
            *const Scalar,
            *const Scalar,
            *const Int,
            *const Int,
            *const Scalar,
            *const Scalar,
            *const Scalar,
            Real,
            *const Real,
            Int,
            *const Scalar,
            *mut Scalar,
        ),
    > = None;

    let id = 1;
    match dim {
        2 => {
            match opt.variable_coefficient {
                CoeffType::COEFF_CIRCLE => {
                    exact_func = circle_u_2d;
                }
                CoeffType::COEFF_CROSS => {
                    exact_func = cross_u_2d;
                }
                CoeffType::COEFF_CHECKERBOARD_0 => {
                    exact_func = zero;
                }
                _ => match periodicity {
                    Some(ref per_l) if per_l[0] > 0. => {
                        if per_l[1] > 0. {
                            exact_func = xytrig_u_2d;
                        } else {
                            exact_func = xtrig_u_2d;
                        }
                    }
                    _ => {
                        exact_func = quadratic_u_2d;
                        exact_field = Some(quadratic_u_field_2d);
                    }
                },
            }

            if opt.bc_type == BCType::NEUMANN {
                let mut label = dm.label("boundary")?.unwrap();
                let bd = dm.add_boundary_natural(
                    "wall",
                    &mut label,
                    slice::from_ref(&id),
                    0,
                    &[],
                    |_, _, _, _, _| Ok(()),
                )?;
                let (mut wf, _, _, _, _, _, _) = dm.try_ds_mut().unwrap().boundary_info(bd)?;
                petsc_weak_form_set_index_bd_residual(&mut wf, &label, id, f0_bd_u)?;
            }
        }
        3 => {
            exact_func = quadratic_u_3d;
            exact_field = Some(quadratic_u_field_3d);

            if opt.bc_type == BCType::NEUMANN {
                let mut label = dm.label("boundary")?.unwrap();
                let bd = dm.add_boundary_natural(
                    "wall",
                    &mut label,
                    slice::from_ref(&id),
                    0,
                    &[],
                    |_, _, _, _, _| Ok(()),
                )?;
                let (mut wf, _, _, _, _, _, _) = dm.try_ds_mut().unwrap().boundary_info(bd)?;
                petsc_weak_form_set_index_bd_residual(&mut wf, &label, id, f0_bd_u)?;
            }
        }
        _ => {
            exact_func = xtrig_u_2d;
            Petsc::set_error(
                dm.comm(),
                ErrorKind::PETSC_ERR_ARG_OUTOFRANGE,
                format!("Invalid dimension {}", dim),
            )?;
        }
    }

    if opt.variable_coefficient == CoeffType::COEFF_CHECKERBOARD_0 {
        dm.try_ds_mut().unwrap().set_constants(&[opt.div, opt.k])?;
    }

    dm.try_ds_mut()
        .unwrap()
        .set_exact_solution(0, exact_func.clone())?;

    if opt.bc_type == BCType::DIRICHLET {
        if let Some(mut label) = dm.label("marker")? {
            if opt.field_bc {
                dm.add_boundary_field_raw(
                    DMBoundaryConditionType::DM_BC_ESSENTIAL_FIELD,
                    "wall",
                    &mut label,
                    slice::from_ref(&id),
                    0,
                    &[],
                    exact_field,
                    None,
                )?;
            }
            let _ = dm.add_boundary_essential(
                "wall",
                &mut label,
                slice::from_ref(&id),
                0,
                &[],
                exact_func.clone(),
            )?;
        } else {
            todo!();
        }
    }

    Ok((exact_func, exact_field))
}

fn setup_discretization(
    dm: &mut DM,
    opt: &Opt,
    kgrid: &Option<Vec<i32>>,
) -> petsc::Result<(
    fn(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>,
    Option<
        unsafe extern "C" fn(
            Int,
            Int,
            Int,
            *const Int,
            *const Int,
            *const Scalar,
            *const Scalar,
            *const Scalar,
            *const Int,
            *const Int,
            *const Scalar,
            *const Scalar,
            *const Scalar,
            Real,
            *const Real,
            Int,
            *const Scalar,
            *mut Scalar,
        ),
    >,
)> {
    let dim = dm.dimension()?;
    // DMConvert(dm, DMPLEX, &plex);
    // DMPlexIsSimplex(plex, &simplex);
    // for now we are just doing this (we assume the dm is a DMPlex)
    let simplex = dm.plex_is_simplex()?;

    let mut fe = FEDisc::create_default(dm.comm(), dim, 1, simplex, None, None)?;
    fe.set_name("potential")?;

    let fe_aux = if opt.variable_coefficient == CoeffType::COEFF_FIELD
        || opt.variable_coefficient == CoeffType::COEFF_CHECKERBOARD_1
    {
        let mut fe_aux = FEDisc::create_default(dm.comm(), dim, 1, simplex, "mat_", None)?;
        fe_aux.set_name("coefficient")?;
        fe_aux.copy_quadrature_from(&fe)?;
        Some(fe_aux)
    } else if opt.field_bc {
        let mut fe_aux = FEDisc::create_default(dm.comm(), dim, 1, simplex, "bc_", None)?;
        fe_aux.copy_quadrature_from(&fe)?;
        Some(fe_aux)
    } else {
        None
    };

    //let _ = dm.clone_without_closures()?;
    dm.add_field(None, fe)?;
    dm.create_ds()?;

    let exact_func_field = setup_problem(dm, opt)?;

    dm.plex_for_each_coarse_dm(|cdm| -> petsc::Result<()> {
        setup_aux_dm(cdm, &fe_aux, opt, kgrid)?;
        if opt.bc_type == BCType::DIRICHLET {
            if !cdm.has_label("marker")? {
                create_bc_label(cdm, "marker", opt)?;
            }
        }
        Ok(())
    })?;

    Ok(exact_func_field)
}

fn create_bc_label(dm: &mut DM, labelname: &str, _opt: &Opt) -> petsc::Result<()> {
    dm.create_label(labelname)?;
    let mut label = dm.label(labelname)?.unwrap();
    // TODO: do convert
    dm.plex_mark_boundary_faces(1, &mut label)
}

fn setup_aux_dm(
    dm: &mut DM,
    fe_aux: &Option<FEDisc>,
    opt: &Opt,
    kgrid: &Option<Vec<i32>>,
) -> petsc::Result<()> {
    let dim = dm.dimension()?;
    let simplex = dm.plex_is_simplex()?;
    let coord_dm = dm.coordinate_dm()?;
    if let Some(fe_aux) = fe_aux {
        // clone the fe_aux
        let this_fe_aux = if opt.variable_coefficient == CoeffType::COEFF_FIELD
            || opt.variable_coefficient == CoeffType::COEFF_CHECKERBOARD_1
        {
            let mut this_fe_aux = FEDisc::create_default(dm.comm(), dim, 1, simplex, "mat_", None)?;
            this_fe_aux.set_name("coefficient")?;
            this_fe_aux.copy_quadrature_from(fe_aux)?;
            this_fe_aux
        } else if opt.field_bc {
            let mut this_fe_aux = FEDisc::create_default(dm.comm(), dim, 1, simplex, "bc_", None)?;
            this_fe_aux.copy_quadrature_from(fe_aux)?;
            this_fe_aux
        } else {
            return Ok(());
        };

        let vec;
        {
            let mut dm_aux = dm.clone_shallow()?;
            dm_aux.set_coordinate_dm(&coord_dm)?;
            dm_aux.add_field(None, this_fe_aux)?;
            dm_aux.create_ds()?;
            vec = if opt.field_bc {
                setup_bc(&mut dm_aux, opt)?
            } else {
                setup_material(&mut dm_aux, opt, kgrid)?
            };
        }

        dm.set_auxiliary_vec(None, 0, 0, vec)?;
    }

    Ok(())
}

fn setup_material<'a>(
    dm_aux: &mut DM<'a, '_>,
    opt: &Opt,
    kgrid: &Option<Vec<i32>>,
) -> petsc::Result<Vector<'a>> {
    let mut nu = dm_aux.create_local_vector()?;
    nu.set_name("Coefficient")?;
    // Rust has trouble knowing we want Box<dyn _> (and not Box<_>) without the explicate type signature.
    // Thus we need to define funcs outside of the `project_function_local` function call.
    let funcs: [Box<dyn FnMut(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>>; 1] =
        [Box::new(|a, b, c, d, e| {
            if opt.variable_coefficient == CoeffType::COEFF_CHECKERBOARD_0 {
                checkerboard_coeff(a, b, c, d, e, (opt, kgrid))
            } else {
                nu_2d(a, b, c, d, e)
            }
        })];
    dm_aux.project_function_local(0.0, InsertMode::INSERT_ALL_VALUES, &mut nu, funcs)?;

    Ok(nu)
}

fn setup_bc<'a>(dm_aux: &mut DM<'a, '_>, _opt: &Opt) -> petsc::Result<Vector<'a>> {
    let mut uexact = dm_aux.create_local_vector()?;
    let funcs: [Box<dyn FnMut(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>>; 1] =
        [Box::new(|dim, b, c, d, e| {
            if dim == 2 {
                quadratic_u_2d(dim, b, c, d, e)
            } else {
                quadratic_u_3d(dim, b, c, d, e)
            }
        })];
    dm_aux.project_function_local(0.0, InsertMode::INSERT_ALL_VALUES, &mut uexact, funcs)?;

    Ok(uexact)
}

fn main() -> petsc::Result<()> {
    let petsc = Petsc::builder()
        .args(std::env::args())
        .help_msg(HELP_MSG)
        .init()?;

    let opt: Opt = petsc.options_with_prefix_and_title(
        None,
        "Poisson Problem (snes-ex12) Options",
        "DMPLEX",
    )?;

    example_12(&petsc, opt)
}

fn example_12(petsc: &Petsc, opt: Opt) -> petsc::Result<()> {
    if opt.show_opts {
        petsc_println!(petsc.world(), "Opts: {:#?}", opt)?;
    }

    let (mut dm, kgrid) = create_mesh(petsc.world(), &opt)?;
    if opt.verbose {
        dm.view_with(None)?;
    }

    let (exact_func, exact_field) = setup_discretization(&mut dm, &opt, &kgrid)?;

    let mut u = dm.create_global_vector()?;
    u.set_name("potential")?;

    #[allow(non_snake_case)]
    let mut J = dm.create_matrix()?;

    let mut a2_mat;
    let mut a2_shell;
    #[allow(non_snake_case)]
    let A2_ref;
    #[allow(non_snake_case)]
    let mut J2 = None;
    #[allow(non_snake_case)]
    let mut A = if opt.jacobian_mf {
        let (mg, ng) = J.global_size()?;
        let (m, n) = J.local_size()?;
        #[allow(non_snake_case)]
        let mut A = Mat::create_shell(J.comm(), m, n, mg, ng, Box::new(()))?;

        let mut u_loc = dm.create_local_vector()?;
        let exact_funcs: [Box<
            dyn FnMut(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>,
        >; 1] = [Box::new(exact_func)];
        if opt.field_bc {
            dm.project_field_local_raw(
                0.0,
                None,
                InsertMode::INSERT_BC_VALUES,
                &mut u_loc,
                [exact_field],
            )?;
        } else {
            dm.project_function_local(0.0, InsertMode::INSERT_BC_VALUES, &mut u_loc, exact_funcs)?;
        }

        if cfg!(cfg_false) {
            A.shell_set_operation_mvv(MatOperation::MATOP_MULT, |_m, _x, _y| todo!())?;
        }

        a2_shell = A.clone();
        A2_ref = a2_shell.deref_mut();
        J2 = Some(J.clone());
        Some(A)
    } else {
        a2_mat = J.clone();
        A2_ref = &mut a2_mat;
        None
    };

    let nullspace = if opt.bc_type == BCType::DIRICHLET {
        let ns = Rc::new(NullSpace::create(dm.comm(), true, vec![])?);
        if let Some(a_mat) = A.as_mut() {
            a_mat.set_nullspace(ns.clone())?;
        }
        Some(ns)
    } else {
        None
    };

    let exact_funcs: [Box<dyn FnMut(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>>;
        1] = [Box::new(exact_func)];
    if opt.field_bc {
        dm.project_field_raw(
            0.0,
            None,
            InsertMode::INSERT_ALL_VALUES,
            &mut u,
            [exact_field],
        )?;
    } else {
        dm.project_function(0.0, InsertMode::INSERT_ALL_VALUES, &mut u, exact_funcs)?;
    }

    if opt.show_initial {
        let mut local = dm.local_vector()?;
        dm.global_to_local(&u, InsertMode::INSERT_VALUES, &mut local)?;
        petsc_println!(petsc.world(), "Local Function:")?;
        petsc_println_sync!(
            petsc.world(),
            "[process {}]\n{:.5}",
            petsc.world().rank(),
            *local.view()?
        )?;
        local.view_with(None)?;
    }

    let mut snes = SNES::create(petsc.world())?;
    snes.set_dm(dm)?;
    snes.set_from_options()?;

    snes.use_dm_plex_local_fem()?;
    if let Some(ref mut a_mat) = A {
        snes.set_jacobian(a_mat.deref_mut(), &mut J, |_, _, _, _| Ok(()))?;
    } else {
        snes.set_jacobian_single_mat(&mut J, |_, _, _| Ok(()))?;
    }

    if opt.run_type == RunType::RUN_FULL || opt.run_type == RunType::RUN_EXACT {
        let initial_guess: Box<
            dyn FnMut(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>,
        > = if opt.nonz_init {
            Box::new(ecks)
        } else {
            Box::new(zero)
        };
        if opt.run_type == RunType::RUN_FULL {
            snes.dm_or_create()?.project_function(
                0.0,
                InsertMode::INSERT_VALUES,
                &mut u,
                [initial_guess],
            )?;
        }
        if opt.guess_vec_view {
            u.view_with(None)?;
        }
        snes.solve(None, &mut u)?;

        if opt.show_solution {
            petsc_println!(petsc.world(), "Solution:")?;
            petsc_println_sync!(
                petsc.world(),
                "[process {}]\n{:.5}",
                petsc.world().rank(),
                *u.view()?
            )?;
            u.view_with(None)?;
        }
    } else if opt.run_type == RunType::RUN_PERF {
        let mut r = snes.dm_or_create()?.create_global_vector()?;
        snes.compute_function(&u, &mut r)?;
        r.chop(1.0e-10)?;
        let norm = r.norm(NormType::NORM_2)?;
        petsc_println!(
            petsc.world(),
            "Initial Residual:\n L_2 Residual: {:.5}",
            norm
        )?;
    } else {
        let tol = 1.0e-11;
        let mut r = snes.dm_or_create()?.create_global_vector()?;
        let exact_funcs: [Box<
            dyn FnMut(Int, Real, &[Real], Int, &mut [Scalar]) -> petsc::Result<()>,
        >; 1] = [Box::new(exact_func)];
        if opt.verbose {
            petsc_println!(petsc.world(), "Initial Guess:")?;
            u.view_with(None)?;
        }
        let error = snes.dm_or_create()?.compute_l2_diff(0.0, &u, exact_funcs)?;
        if error < tol && opt.verbose {
            petsc_println!(petsc.world(), "L_2 Error: < {:.1e}", tol)?;
        } else if error >= tol {
            petsc_println!(petsc.world(), "L_2 Error: {:.5e}", error)?;
        }

        snes.compute_function(&u, &mut r)?;

        r.chop(1.0e-10)?;
        if opt.verbose {
            petsc_println!(petsc.world(), "Initial Residual:\n")?;
            r.view_with(None)?;
        }
        let norm = r.norm(NormType::NORM_2)?;
        if opt.verbose {
            petsc_println!(petsc.world(), "L_2 Residual: {:.5}", norm)?;
        }

        {
            snes.compute_jacobian(&u, A2_ref, None)?;
            let mut b = u.clone();
            r.set_all(Scalar::from(0.0))?;
            snes.compute_function(&r, &mut b)?;
            A2_ref.mult(&u, &mut r)?;
            r.axpy(Scalar::from(1.0), &b)?;
            if opt.verbose {
                petsc_println!(petsc.world(), "Au - b = Au + F(0)")?;
            }
            r.chop(1.0e-10)?;
            if opt.verbose {
                r.view_with(None)?;
            }
            let norm = r.norm(NormType::NORM_2)?;
            if opt.verbose {
                petsc_println!(petsc.world(), "Linear L_2 Residual: {:.5}", norm)?;
            }

            if opt.check_ksp {
                if let Some(ns) = nullspace {
                    ns.remove_from(&mut u)?;
                }

                let (a_mat, j_mat) = if let Some(j_mat) = J2.as_mut() {
                    snes.compute_jacobian(&u, A2_ref, &mut *j_mat)?;
                    (&*A2_ref, &*j_mat)
                } else {
                    snes.compute_jacobian(&u, A2_ref, None)?;
                    (&*A2_ref, &*A2_ref)
                };

                a_mat.mult(&u, &mut b)?;
                let mut ksp = snes.ksp_or_create()?;
                ksp.set_operators(a_mat, j_mat)?;
                ksp.solve(&b, &mut r)?;
                r.axpy(Scalar::from(-1.0), &u)?;
                let res = r.norm(NormType::NORM_2)?;
                if opt.verbose {
                    petsc_println!(petsc.world(), "KSP Error: {}", res)?;
                }
            }
        }
    }

    if opt.vec_view {
        u.view_with(None)?;
    }
    if opt.coeff_view {
        if let Some(nu) = snes.dm_or_create()?.auxiliary_vec(None, 0, 0)? {
            nu.view_with(None)?
        }
    }

    if opt.bd_integral {
        let exact = 10.0 / 3.0;
        let dm = snes.dm_or_create()?;
        let l = dm.label("marker")?;

        let bd_int =
            dm.plex_compute_bd_integral_raw(&u, l.as_ref(), slice::from_ref(&1), bd_integral_2d)?;
        #[cfg(feature = "petsc-use-complex-unsafe")]
        let bd_int_real_abs = bd_int.norm();
        #[cfg(not(feature = "petsc-use-complex-unsafe"))]
        let bd_int_real_abs = bd_int.abs();
        petsc_println!(petsc.world(), "Solution boundary integral: {:.4}", bd_int)?;
        if (bd_int_real_abs - exact).abs() > Real::EPSILON.sqrt() {
            Petsc::set_error(
                petsc.world(),
                ErrorKind::PETSC_ERR_PLIB,
                format!("Invalid boundary integral {} != {}", bd_int_real_abs, exact),
            )?;
        }
    }

    Ok(())
}

const PETSC_PI: Real = std::f64::consts::PI as Real;

fn zero(_dim: Int, _time: Real, _x: &[Real], _nc: Int, u: &mut [Scalar]) -> petsc::Result<()> {
    u[0] = Scalar::from(0.0);
    Ok(())
}

fn ecks(_dim: Int, _time: Real, x: &[Real], _nc: Int, u: &mut [Scalar]) -> petsc::Result<()> {
    u[0] = Scalar::from(x[0]);
    Ok(())
}

/*
  In 2D for Dirichlet conditions, we use exact solution:

    u = x^2 + y^2
    f = 4

  so that

    -\Delta u + f = -4 + 4 = 0

  For Neumann conditions, we have

    -\nabla u \cdot -\hat y |_{y=0} =  (2y)|_{y=0} =  0 (bottom)
    -\nabla u \cdot  \hat y |_{y=1} = -(2y)|_{y=1} = -2 (top)
    -\nabla u \cdot -\hat x |_{x=0} =  (2x)|_{x=0} =  0 (left)
    -\nabla u \cdot  \hat x |_{x=1} = -(2x)|_{x=1} = -2 (right)

  Which we can express as

    \nabla u \cdot  \hat n|_\Gamma = {2 x, 2 y} \cdot \hat n = 2 (x + y)

  The boundary integral of this solution is (assuming we are not orienting the edges)

    \int^1_0 x^2 dx + \int^1_0 (1 + y^2) dy + \int^1_0 (x^2 + 1) dx + \int^1_0 y^2 dy = 1/3 + 4/3 + 4/3 + 1/3 = 3 1/3
*/
fn quadratic_u_2d(
    _dim: Int,
    _time: Real,
    x: &[Real],
    _nc: Int,
    u: &mut [Scalar],
) -> petsc::Result<()> {
    u[0] = Scalar::from(x[0] * x[0] + x[1] * x[1]);
    Ok(())
}

unsafe extern "C" fn quadratic_u_field_2d(
    _dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    uexact: *mut Scalar,
) {
    *uexact = *a;
}

fn circle_u_2d(
    _dim: Int,
    _time: Real,
    x: &[Real],
    _nc: Int,
    u: &mut [Scalar],
) -> petsc::Result<()> {
    let alpha = Scalar::from(500.0);
    let radius2 = Real::powi(0.15, 2);
    let r2 = (x[0] - 0.5).powi(2) + (x[1] - 0.5).powi(2);
    let xi = alpha * (radius2 - r2);

    u[0] = Scalar::tanh(xi) + 1.0;
    Ok(())
}

fn cross_u_2d(_dim: Int, _time: Real, x: &[Real], _nc: Int, u: &mut [Scalar]) -> petsc::Result<()> {
    let alpha = 50.0 * 4.0;
    let xy = (x[0] - 0.5) * (x[1] - 0.5);
    u[0] = Scalar::from(
        Real::sin(alpha * xy)
            * if alpha * xy.abs() < 2.0 * PETSC_PI {
                1.0
            } else {
                0.01
            },
    );
    Ok(())
}

unsafe extern "C" fn f0_u(
    _dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    *f0 = Scalar::from(4.0);
}

unsafe extern "C" fn f0_circle_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    let f0 = slice::from_raw_parts_mut(f0, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);

    let alpha = Scalar::from(500.0);
    let radius2 = Real::powi(0.15, 2);
    let r2 = (x[0] - 0.5).powi(2) + (x[1] - 0.5).powi(2);
    let xi = alpha * (radius2 - r2);

    f0[0] = (-4.0 * alpha - 8.0 * alpha.powi(2) * r2 * Scalar::tanh(xi))
        * (1.0 / Scalar::cosh(xi)).powi(2);
}

unsafe extern "C" fn f0_cross_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    let f0 = slice::from_raw_parts_mut(f0, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);

    let alpha = 50.0 * 4.0;
    let xy = (x[0] - 0.5) * (x[1] - 0.5);

    f0[0] = Scalar::from(
        Real::sin(alpha * xy)
            * if alpha * xy.abs() < 2.0 * PETSC_PI {
                1.0
            } else {
                0.01
            },
    );
}

unsafe extern "C" fn f0_checkerboard_0_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    let f0 = slice::from_raw_parts_mut(f0, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);

    f0[0] = Scalar::from(-20.0) * Real::exp(-((x[0] - 0.5).powi(2) + (x[1] - 0.5).powi(2)));
}

unsafe extern "C" fn f0_bd_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    n: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    let f0 = slice::from_raw_parts_mut(f0, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);
    let n = slice::from_raw_parts(n, dim as usize);

    f0[0] = (0..dim as usize).fold(Scalar::from(0.0), |res, d| res + -n[d] * 2.0 * x[d]);
}

/* gradU[comp*dim+d] = {u_x, u_y} or {u_x, u_y, u_z} */
unsafe extern "C" fn f1_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f1: *mut Scalar,
) {
    let f1 = slice::from_raw_parts_mut(f1, dim as usize);
    let u_x = slice::from_raw_parts(u_x, dim as usize);

    for d in 0..dim as usize {
        f1[d] = u_x[d];
    }
}

/* < \nabla v, \nabla u + {\nabla u}^T >
This just gives \nabla u, give the perdiagonal for the transpose */
unsafe extern "C" fn g3_uu(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _u_t_shift: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    g3: *mut Scalar,
) {
    let g3 = slice::from_raw_parts_mut(g3, (dim * dim) as usize);

    for d in 0..dim as usize {
        g3[d * dim as usize + d] = Scalar::from(1.0);
    }
}

/*
  In 2D for x periodicity and y Dirichlet conditions, we use exact solution:

    u = sin(2 pi x)
    f = -4 pi^2 sin(2 pi x)

  so that

    -\Delta u + f = 4 pi^2 sin(2 pi x) - 4 pi^2 sin(2 pi x) = 0
*/
fn xtrig_u_2d(_dim: Int, _time: Real, x: &[Real], _nc: Int, u: &mut [Scalar]) -> petsc::Result<()> {
    u[0] = Scalar::sin(Scalar::from(2.0) * PETSC_PI * x[0]);
    Ok(())
}

unsafe extern "C" fn f0_xtrig_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    let f0 = slice::from_raw_parts_mut(f0, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);

    f0[0] = Scalar::from(-4.0) * PETSC_PI.powi(2) * Real::sin(2.0 * PETSC_PI * x[0]);
}

/*
  In 2D for x-y periodicity, we use exact solution:

    u = sin(2 pi x) sin(2 pi y)
    f = -8 pi^2 sin(2 pi x)

  so that

    -\Delta u + f = 4 pi^2 sin(2 pi x) sin(2 pi y) + 4 pi^2 sin(2 pi x) sin(2 pi y) - 8 pi^2 sin(2 pi x) = 0
*/
fn xytrig_u_2d(
    _dim: Int,
    _time: Real,
    x: &[Real],
    _nc: Int,
    u: &mut [Scalar],
) -> petsc::Result<()> {
    u[0] = Scalar::sin(Scalar::from(2.0) * PETSC_PI * x[0])
        * Scalar::sin(Scalar::from(2.0) * PETSC_PI * x[1]);
    Ok(())
}

unsafe extern "C" fn f0_xytrig_u(
    _dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    *f0 = Scalar::from(-8.0) * PETSC_PI.powi(2) * Real::sin(2.0 * PETSC_PI * (*x));
}

/*
  In 2D for Dirichlet conditions with a variable coefficient, we use exact solution:

    u  = x^2 + y^2
    f  = 6 (x + y)
    nu = (x + y)

  so that

    -\div \nu \grad u + f = -6 (x + y) + 6 (x + y) = 0
*/
fn nu_2d(_dim: Int, _time: Real, x: &[Real], _nc: Int, u: &mut [Scalar]) -> petsc::Result<()> {
    u[0] = Scalar::from(x[0] + x[1]);
    Ok(())
}

fn checkerboard_coeff(
    dim: Int,
    _time: Real,
    x: &[Real],
    _nc: Int,
    u: &mut [Scalar],
    ctx: (&Opt, &Option<Vec<Int>>),
) -> petsc::Result<()> {
    let (opt, kgrid) = ctx;
    let k;
    let mask = (0..dim as usize).fold(0, |mask, d| (mask + (x[d] * opt.div as Real) as Int) % 2);
    if let Some(kgrid) = kgrid {
        let ind = (0..dim as usize).fold(0, |mut ind, d| {
            if d > 0 {
                ind *= dim
            }
            ind + (x[d] * opt.div as Real) as Int
        });
        k = kgrid[ind as usize];
    } else {
        k = opt.k;
    }
    u[0] = if mask == 1 {
        Scalar::from(1.0)
    } else {
        Scalar::from(10.0).powi(-k)
    };

    Ok(())
}

unsafe extern "C" fn f0_analytic_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    let f0 = slice::from_raw_parts_mut(f0, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);

    f0[0] = Scalar::from(6.0) * (x[0] + x[1]);
}

/* gradU[comp*dim+d] = {u_x, u_y} or {u_x, u_y, u_z} */
unsafe extern "C" fn f1_analytic_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f1: *mut Scalar,
) {
    let f1 = slice::from_raw_parts_mut(f1, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);
    let u_x = slice::from_raw_parts(u_x, dim as usize);

    for d in 0..dim as usize {
        f1[d] = (x[0] + x[1]) * u_x[d];
    }
}

unsafe extern "C" fn f1_field_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f1: *mut Scalar,
) {
    let f1 = slice::from_raw_parts_mut(f1, dim as usize);
    let a = slice::from_raw_parts(a, dim as usize);
    let u_x = slice::from_raw_parts(u_x, dim as usize);

    for d in 0..dim as usize {
        f1[d] = a[0] * u_x[d];
    }
}

/* < \nabla v, \nabla u + {\nabla u}^T >
This just gives \nabla u, give the perdiagonal for the transpose */
unsafe extern "C" fn g3_analytic_uu(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _u_t_shift: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    g3: *mut Scalar,
) {
    let g3 = slice::from_raw_parts_mut(g3, (dim * dim) as usize);
    let x = slice::from_raw_parts(x, dim as usize);

    for d in 0..dim as usize {
        g3[d * dim as usize + d] = Scalar::from(x[0] + x[1]);
    }
}

unsafe extern "C" fn g3_field_uu(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _u_t_shift: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    g3: *mut Scalar,
) {
    let g3 = slice::from_raw_parts_mut(g3, (dim * dim) as usize);
    let a = slice::from_raw_parts(a, dim as usize);

    for d in 0..dim as usize {
        g3[d * dim as usize + d] = a[0];
    }
}

/*
  In 2D for Dirichlet conditions with a nonlinear coefficient (p-Laplacian with p = 4), we use exact solution:

    u  = x^2 + y^2
    f  = 16 (x^2 + y^2)
    nu = 1/2 |grad u|^2

  so that

    -\div \nu \grad u + f = -16 (x^2 + y^2) + 16 (x^2 + y^2) = 0
*/
unsafe extern "C" fn f0_analytic_nonlinear_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f0: *mut Scalar,
) {
    let f0 = slice::from_raw_parts_mut(f0, dim as usize);
    let x = slice::from_raw_parts(x, dim as usize);

    f0[0] = Scalar::from(16.0) * (x[0] * x[0] + x[1] * x[1]);
}

/* gradU[comp*dim+d] = {u_x, u_y} or {u_x, u_y, u_z} */
unsafe extern "C" fn f1_analytic_nonlinear_u(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    f1: *mut Scalar,
) {
    let f1 = slice::from_raw_parts_mut(f1, dim as usize);
    let u_x = slice::from_raw_parts(u_x, dim as usize);

    let nu = (0..dim as usize).fold(Scalar::from(0.0), |nu, d| nu + u_x[d] * u_x[d]);
    for d in 0..dim as usize {
        f1[d] = 0.5 * nu * u_x[d];
    }
}

/*
  grad (u + eps w) - grad u = eps grad w

  1/2 |grad (u + eps w)|^2 grad (u + eps w) - 1/2 |grad u|^2 grad u
= 1/2 (|grad u|^2 + 2 eps <grad u,grad w>) (grad u + eps grad w) - 1/2 |grad u|^2 grad u
= 1/2 (eps |grad u|^2 grad w + 2 eps <grad u,grad w> grad u)
= eps (1/2 |grad u|^2 grad w + grad u <grad u,grad w>)
*/
unsafe extern "C" fn g3_analytic_nonlinear_uu(
    dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _u_t_shift: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    g3: *mut Scalar,
) {
    let g3 = slice::from_raw_parts_mut(g3, (dim * dim) as usize);
    let u_x = slice::from_raw_parts(u_x, dim as usize);

    let nu = (0..dim as usize).fold(Scalar::from(0.0), |nu, d| nu + u_x[d] * u_x[d]);
    for d in 0..dim as usize {
        g3[d * dim as usize + d] = Scalar::from(0.5) * nu;
        for e in 0..dim as usize {
            g3[d * dim as usize + e] += u_x[d] * u_x[e];
        }
    }
}

/*
  In 3D for Dirichlet conditions we use exact solution:

    u = 2/3 (x^2 + y^2 + z^2)
    f = 4

  so that

    -\Delta u + f = -2/3 * 6 + 4 = 0

  For Neumann conditions, we have

    -\nabla u \cdot -\hat z |_{z=0} =  (2z)|_{z=0} =  0 (bottom)
    -\nabla u \cdot  \hat z |_{z=1} = -(2z)|_{z=1} = -2 (top)
    -\nabla u \cdot -\hat y |_{y=0} =  (2y)|_{y=0} =  0 (front)
    -\nabla u \cdot  \hat y |_{y=1} = -(2y)|_{y=1} = -2 (back)
    -\nabla u \cdot -\hat x |_{x=0} =  (2x)|_{x=0} =  0 (left)
    -\nabla u \cdot  \hat x |_{x=1} = -(2x)|_{x=1} = -2 (right)

  Which we can express as

    \nabla u \cdot  \hat n|_\Gamma = {2 x, 2 y, 2z} \cdot \hat n = 2 (x + y + z)
*/
fn quadratic_u_3d(
    _dim: Int,
    _time: Real,
    x: &[Real],
    _nc: Int,
    u: &mut [Scalar],
) -> petsc::Result<()> {
    u[0] = Scalar::from(2.0) * (x[0] * x[0] + x[1] * x[1] + x[2] * x[2]) / 3.0;
    Ok(())
}

unsafe extern "C" fn quadratic_u_field_3d(
    _dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    _u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _x: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    uexact: *mut Scalar,
) {
    *uexact = *a;
}

unsafe extern "C" fn bd_integral_2d(
    _dim: Int,
    _nf: Int,
    _nf_aux: Int,
    _u_off: *const Int,
    _u_off_x: *const Int,
    u: *const Scalar,
    _u_t: *const Scalar,
    _u_x: *const Scalar,
    _a_off: *const Int,
    _a_off_x: *const Int,
    _a: *const Scalar,
    _a_t: *const Scalar,
    _a_x: *const Scalar,
    _t: Real,
    _x: *const Real,
    _n: *const Real,
    _nc: Int,
    _consts: *const Scalar,
    uint: *mut Scalar,
) {
    *uint = *u;
}

// ----------------------------------------------------------------------------
// Tests
// ----------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn snes_example_12() {
        let petsc = Petsc::init_no_args().expect("failed to initalize PETSc");

        // Default run
        let opt = Opt {
            run_type: RunType::RUN_TEST,
            bc_type: BCType::DIRICHLET,
            variable_coefficient: CoeffType::COEFF_NONE,
            field_bc: false,
            jacobian_mf: false,
            show_initial: false,
            show_solution: false,
            nonz_init: false,
            bd_integral: false,
            check_ksp: true,
            div: 4,
            k: 1,
            rand: false,
            dm_view: false,
            guess_vec_view: false,
            vec_view: false,
            coeff_view: false,
            show_opts: false,
            verbose: false,
        };
        assert!(example_12(&petsc, opt).is_ok());

        // Non-zero initial
        let opt = Opt {
            run_type: RunType::RUN_TEST,
            bc_type: BCType::DIRICHLET,
            variable_coefficient: CoeffType::COEFF_NONE,
            field_bc: false,
            jacobian_mf: false,
            show_initial: false,
            show_solution: false,
            nonz_init: true,
            bd_integral: false,
            check_ksp: true,
            div: 4,
            k: 1,
            rand: false,
            dm_view: false,
            guess_vec_view: false,
            vec_view: false,
            coeff_view: false,
            show_opts: false,
            verbose: false,
        };
        assert!(example_12(&petsc, opt).is_ok());

        // Random k values
        let opt = Opt {
            run_type: RunType::RUN_TEST,
            bc_type: BCType::DIRICHLET,
            variable_coefficient: CoeffType::COEFF_NONE,
            field_bc: false,
            jacobian_mf: false,
            show_initial: false,
            show_solution: false,
            nonz_init: false,
            bd_integral: false,
            check_ksp: true,
            div: 4,
            k: 1,
            rand: true,
            dm_view: false,
            guess_vec_view: false,
            vec_view: false,
            coeff_view: false,
            show_opts: false,
            verbose: false,
        };
        assert!(example_12(&petsc, opt).is_ok());
    }
}

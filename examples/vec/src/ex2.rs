//! This file will show how to do the vec ex1 example in rust using the petsc-rs bindings.
//!
//! Concepts: vectors^assembling vectors;
//! Processors: n
//!
//! Use "petsc::prelude::*" to get direct access to all important petsc-rs bindings
//!     and mpi traits which allow you to call things like `world.size()`.
//!
//! To run:
//! ```text
//! $ cargo build --bin vec-ex2
//! $ mpiexec -n 1 target/debug/vec-ex2
//! $ mpiexec -n 5 target/debug/vec-ex2
//! ```

static HELP_MSG: &'static str =
    "Builds a parallel vector with 1 component on the first processor, 2 on the second, etc.\n\
    Then each processor adds one to all elements except the last rank.\n\n";

use mpi::traits::*;
use petsc::prelude::*;

struct Opt {
    verbose: bool,
}

impl petsc::Opt for Opt {
    fn from_opt_builder(pob: &mut petsc::OptBuilder) -> petsc::Result<Self> {
        let verbose = pob.options_bool("-verbose", "", "snes-ex2", true)?;
        Ok(Opt { verbose })
    }
}

fn main() -> petsc::Result<()> {
    // optionally initialize mpi
    // let _univ = mpi::initialize().unwrap();
    // init with no options
    let petsc = Petsc::builder()
        .args(std::env::args())
        .help_msg(HELP_MSG)
        .init()?;

    // or init with no options
    // let petsc = Petsc::init_no_args()?;

    let opt = petsc.options()?;

    example_02(&petsc, opt)
}

fn example_02(petsc: &Petsc, opt: Opt) -> petsc::Result<()> {
    let Opt { verbose } = opt;

    let rank = petsc.world().rank() as Int;

    let mut x = petsc.vec_create()?;
    x.set_sizes((rank + 1) as usize, None)?;
    x.set_from_options()?;
    let size = x.global_size()?;
    x.set_all(Scalar::from(1.0))?;
    // x.set_all(Scalar {re: 1.0, im: 1.0})?;

    x.assemble_with(
        (0..size as Int - rank).map(|i| (i, Scalar::from(1.0))),
        InsertMode::ADD_VALUES,
    )?;

    let viewer = Viewer::create_ascii_stdout(petsc.world())?;
    if verbose {
        x.view_with(Some(&viewer))?;
    }

    // return
    Ok(())
}

// ----------------------------------------------------------------------------
// Tests
// ----------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn vec_example_02() {
        let petsc = Petsc::init_no_args().expect("failed to initalize PETSc");

        let opt = Opt { verbose: false };
        assert!(example_02(&petsc, opt).is_ok());
    }
}
